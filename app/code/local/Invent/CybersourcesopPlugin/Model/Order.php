<?php
/**
 * Magento Cybersource SOP Plugin Extension
 *
 * THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE
 * OR THAT THE USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR
 * OTHER RIGHTS.
 *
 * COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE
 * OF THE SOFTWARE OR DOCUMENTATION.
 *
 * @author Invent Commerce http://www.inventcommerce.com <peter.youell@inventcommerce.com>
 * @copyright Copyright (C) 2015 Invent Commerce
 * All rights reserved
 *
 */

class Invent_CybersourcesopPlugin_Model_Order extends Invent_Cybersourcesop_Model_Soapapi_Order
{
    /*
     * Put your logic here to decided if decision manager is called for a particular transaction.
     *
     * Return True to call decision manager
     * Return False to not call decision manager
     *
     */
    public function enableDM($orderref) {
        //note: $this->getDMConfig() returns the system.xml config value if Decision Manager is enabled or not by default.
        if ($this->getDMConfig()) {
            // Decision Manager is enabled in system configuration

            /* In this Example we are checking if the Grand Total is more than a minimum amount set up in our System Configuration for Decision Manager.
             *
             * If the Grand total is less than the minimum amount Decision Manager will not be called as the method checkMinimumAmount will return False.
             *
             * In all other cases if Decision Manager is enabled in System Configuration and the Grand Total exceeds the minimum amount Decision Manager
             * will be used as checkMinimumAmount will return true.
             */
            $status = Mage::getModel('cybersourcesopplugin/minimumAmount')->checkMinimumAmount($this->getOrder($orderref));
            return $status;
        }
        // Decision Manager is disabled in system configuration
        return false;
    }


    /*
     * Put your logic here to include additional Merchant Defined Fields.
     *
     * Return an array with the key as a numeric index for your Merchant Field and its Value, your value you want to submit.
     * Example data:
     * -------------
        $return[0] = 'custom value 1';
        $return[1] = 'custom value 2';
        $return[2] = 'custom value 3';
        $return[3] = 'custom value 4';
     */
    public function getAdditonalMerchantFields($orderref) {
        $return = array();
        //to access the order object:
        // $order = $this->getOrder($orderref);   /* $orderref is the order ID passed from the process method within magento */
        /*  - Example data
        $return[0] = 'custom value 1';
        $return[1] = 'custom value 2';
        $return[2] = 'custom value 3';
        $return[3] = 'custom value 4';
        */
        return $return;
    }

    /*
     * Put your logic here to decided if decision manager's merchant defined fields are to be included in your transaction.
     *
     * Return True to include Merchant Defined Fields
     * Return False to not include Merchant Defined Fields
     *
     * This function must return True in order to use Additional Merchant Defined Fields.
     *
     * $orderref is included to access the Order Object via $this->getOrder($orderref);
     */
    public function enableMDD($orderref) {
        if ($this->getMDDConfig()) {
            return true;
        } else {
            return false;
        }
    }
}
