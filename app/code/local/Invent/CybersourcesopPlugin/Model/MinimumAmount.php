<?php

class Invent_CybersourcesopPlugin_Model_MinimumAmount extends Mage_Core_Model_Abstract {

    //this returns true or false if the grand total of an order is smaller than the minimum amount
    public function checkMinimumAmount($order) {
        //get system config fields
        $minimumAmount = Mage::getStoreConfig('payment/cybersourcesop/minimum_amount');
        $useShipping = Mage::getStoreConfig('payment/cybersourcesop/include_shipping');
        //get the grand total
        $grandTotal = $order->getGrandTotal();
        //are we including shipping costs in our decision making
        if ($useShipping) {
            $grandTotal -= $order->getShippingAmount();
            $grandTotal -= $order->getShippingTaxAmount();
        }
        //Trigger Decision Manager when an order's grand total is under a certain amount.
        if ($grandTotal < $minimumAmount) {
            return false;
        }
        return true;
    }
}