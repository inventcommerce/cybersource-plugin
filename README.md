# README #

This is an example CybersourceSOP 3rd Party Plugin extension compatible with the latest V3.0.0 of the CybersourceSOP Module by InventCommerce Ltd.

The purpose of this module is to illustrate how to extend the CybersourceSOP Module functionality pertaining to Cybersource Decision Manager's outcome of a particular transaction and the inclusion of Merchant Defined Fields in your transactions. 

In this module you will see a simple example of how to enable / disable Decision Manager for Order's grand totals that exceed a minimum amount. This minimum amount is set up within System Configuration and Shipping Tax can be included / excluded in the Grand Total of the transaction.

Also included is example functionality to enable / disable Merchant Defined Fields and the adding of Additional Merchant Defined fields onto a transaction.

The enabling and disabling of Merchant Defined Fields works in the same way that Decision Manager's enabling and disabling works. 

By Enabling Merchant Defined Fields, the index / key will start at the first element chosen within your System Configuration and any Additional Merchant Defined Fields specified will be added onto the already defined fields chosen in System Configuration.

If no fields are included in system configuration and merchant defined fields are enabled, the index / key will start at 0. You can specify your own index / key values in an Array returned from addAdditionalMerchantFields().